These scripts scrape articles from [The New York Times](https://www.nytimes.com) and write them to disk as text-only html files. You really won't miss the pictures: 95% of the time, they're either stock header images or photos of whatever politician is the subject of the article standing around or looking into space.

Running `./nytimes.py` will download the top ten articles from the front page of nytimes.com to the current working directory. `./daily-nytimes` will download them to the directory in the environment variable `$NYTIMES_DL_LOCATION/$year-$month-$day`. Adding this script to your crontab will give a local copy of the top stories everyday. These copies have several advantages over visiting the website directly:

* They load instantly.
* They are available offline.
* They render nicely in terminal browser such as w3m and lynx.
* They don't have any annoying frills.

