#!/usr/bin/env python3

import sys
import urllib
from urllib import request
from bs4 import BeautifulSoup
import tempfile
import datetime

N_ARTICLES = 10

def fetch_article(article_url, write=True):
	soup = BeautifulSoup(request.urlopen(article_url).read(), 'html.parser')
	title = soup.find("title").text.replace(" - The New York Times", "")
	if "nytimes.com/interactive" in article_url:
		article = "<div>This article contains interactive elements and can't be displayed here. <a href='" + article_url + "'>View Article on nytimes.com</a></div>"
	else:
		article = "".join(str(x) for x in soup.findAll("div", {"class" : "StoryBodyCompanionColumn"}))
	if len(str(article).split()) < 5:	
		article = "<div>An unknown error ocurred while processing this article. <a href='" + article_url + "'>View Article on nytimes.com</a></div>"
	output = """
		<!doctype html>
		<html>
			<head>
				<title>$TITLE - The New York Times</title>
				<meta charset='utf-8'>
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<style>
					body {
						max-width: 700px;
						margin: auto;
						font-size: 12pt;
						padding: 5px;
					}
					p {
						line-height: 1.6;
					}
				</style>
			</head>
			<body lang="en">
				<h1>$TITLE</h1>
				$ARTICLE
			</body>
		</html>
		""".replace("$ARTICLE", article).replace("$TITLE", title)

	if not write:
		return output

	output_file = article_url.split("/")[-1]
		
	with open(output_file, "w") as f:
		f.write(output)
	
def fetch_headlines(date, n_articles=N_ARTICLES):
	soup = BeautifulSoup(request.urlopen("/".join(["https://www.nytimes.com/issue/todayspaper", str(date.year), '%02d' % date.month, '%02d' % date.day, "todays-new-york-times"])).read(), "html.parser")
	headline_elements = soup.findAll("h2", {"class" : "headline"})[0:n_articles]
	for e in headline_elements:
		fetch_article(e.a["href"])
		e.a["href"] = "./" + e.a["href"].split("/")[-1]
	headlines = "".join(str(x) for x in headline_elements)
	with open("index.html", "w") as f:
		f.write("""
			<!doctype html>
			<html>
				<head>
					<title>The New York Times</title>
					<meta charset="utf-8">
				</head>
				<body>
					<h1>The New York Times</h1>
					$HEADLINES
				</body>
			</html>
		""".replace("$HEADLINES", headlines))

if __name__ == "__main__":
    n = 10
    if len(sys.argv) > 1:
        n = int(sys.argv[1])
    fetch_headlines(datetime.datetime.now(), n)
			
