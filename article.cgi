#!/usr/bin/python3

import nytimes
import sys
import cgi

def enc_print(string='', encoding='utf8'):
    sys.stdout.buffer.write(string.encode(encoding) + b'\n')

field = cgi.FieldStorage() 


try:
	
	content = nytimes.fetch_article(field.getvalue("url"), write=False).replace("\n", "").replace("\t","")
except Exception as e: 
	enc_print(e)


enc_print("Content-type:text/html\r\n\r\n")
try:
	enc_print(content)
except Exception as e: 
	enc_print(e)

