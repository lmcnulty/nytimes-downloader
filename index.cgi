#!/usr/bin/env python3

import nytimes
import sys
from bs4 import BeautifulSoup
from urllib import request

def enc_print(string='', encoding='utf8'):
    sys.stdout.buffer.write(string.encode(encoding) + b'\n')

times = BeautifulSoup(request.urlopen("https://www.nytimes.com/").read(), 'html.parser')

enc_print("Content-type: text/html\n")

enc_print(
"""<!doctype html>
<html>
	<head>
		<title>The New York Times: Live Update</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		<h1>The New York Times</h1>""")

for e in times.find_all("div", {"class", "css-6p6lnl"}):
	try:
		enc_print("\t\t<h2><a href='article.cgi?url=https://nytimes.com/" + e.a['href'] + "'>" + e.h2.text + "</a></h2>")
	except Exception as e:
		enc_print(e)

enc_print(
"""	</body>
</html>
""")

